<?php
/**
 * @file
 * Drupal module core file.
 *
 * Defines module paths as constants, permissions, routes as menu and callback
 *   items.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

define('CDSTAR_CONFIG_DEFAULT', 'admin/config/rdp/cdstar');
define('CDSTAR_CONFIG_DEFAULT_EDIT', CDSTAR_CONFIG_DEFAULT . '/edit');
define('CDSTAR_CONFIG_SERVER_DEFAULT', CDSTAR_CONFIG_DEFAULT . '/server');
define('CDSTAR_CONFIG_SERVER_NEW', CDSTAR_CONFIG_SERVER_DEFAULT . '/new');
define('CDSTAR_CONFIG_SERVER_EDIT', CDSTAR_CONFIG_SERVER_DEFAULT . '/%/edit');
define('CDSTAR_CONFIG_SERVER_DELETE', CDSTAR_CONFIG_SERVER_DEFAULT . '/%/delete');
define('CDSTAR_CONFIG_OBJECT_DEFAULT', CDSTAR_CONFIG_DEFAULT . '/object');
define('CDSTAR_CONFIG_OBJECT_NEW', CDSTAR_CONFIG_OBJECT_DEFAULT . '/new');
define('CDSTAR_CONFIG_OBJECT_VIEW', CDSTAR_CONFIG_OBJECT_DEFAULT . '/%');
define('CDSTAR_CONFIG_OBJECT_DELETE', CDSTAR_CONFIG_OBJECT_VIEW . '/delete');
define('CDSTAR_CONFIG_FILE_DOWNLOAD', CDSTAR_CONFIG_OBJECT_VIEW . '/%');
define('CDSTAR_CONFIG_FILE_DELETE', CDSTAR_CONFIG_OBJECT_VIEW . '/%/delete');

define("CDSTAR_PERMISSION_CONFIGURATION", "cdstar configuration");

/**
 * Implements @see hook_permission().
 */
function cdstar_permission() {
  return [
    'cdstar configuration' => [
      'title' => t('CDSTAR configuration'),
      'description' => t('Permission to configure the CDSTAR module.'),
    ],
  ];
}

/**
 * Implements @see hook_menu()
 *
 * @return array
 */
function cdstar_menu() {
  $items = [];

  $items[CDSTAR_CONFIG_DEFAULT] = [
    'title' => t('CDSTAR Object Storage management'),
    'description' => t('CDSTAR module configuration.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => ['cdstar_admin_conf'],
    'access arguments' => [CDSTAR_PERMISSION_CONFIGURATION],
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/admin_conf.inc',
  ];

  $items[CDSTAR_CONFIG_DEFAULT_EDIT] = [
    'title' => t('Configuration'),
    'description' => t('CDSTAR module configuration.'),
    'access arguments' => [CDSTAR_PERMISSION_CONFIGURATION],
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  ];

  $items[CDSTAR_CONFIG_SERVER_DEFAULT] = [
    'title' => t('CDSTAR Servers'),
    'description' => t('Manage CDSTAR configuration.'),
    'page callback' => 'cdstar_admin_conf_server',
    'access arguments' => [CDSTAR_PERMISSION_CONFIGURATION],
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/admin_conf_server.inc',
    'weight' => 10,
  ];

  $items[CDSTAR_CONFIG_OBJECT_DEFAULT] = [
    'title' => t('CDSTAR Objects'),
    'description' => t('Manage CDSTAR objects.'),
    'page callback' => 'cdstar_admin_conf_object',
    'access arguments' => [CDSTAR_PERMISSION_CONFIGURATION],
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/admin_conf_object.inc',
    'weight' => 20,
  ];

  $items[CDSTAR_CONFIG_SERVER_NEW] = [
    'title' => t('Add new CDSTAR Server'),
    'description' => t('Add new CDSTAR Server'),
    'page callback' => 'drupal_get_form',
    'page arguments' => ['cdstar_admin_conf_server_new'],
    'access arguments' => [CDSTAR_PERMISSION_CONFIGURATION],
    'type' => MENU_CALLBACK,
    'file' => 'includes/admin_conf_server.inc',
  ];
  $items[CDSTAR_CONFIG_SERVER_EDIT] = [
    'title' => 'Edit CDSTAR server',
    'description' => '',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['cdstar_admin_conf_server_edit', 5],
    'access arguments' => [CDSTAR_PERMISSION_CONFIGURATION],
    'type' => MENU_CALLBACK,
    'file' => 'includes/admin_conf_server.inc',
  ];
  $items[CDSTAR_CONFIG_SERVER_DELETE] = [
    'title' => 'Edit CDSTAR server',
    'description' => '',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['cdstar_admin_conf_server_delete', 5],
    'access arguments' => [CDSTAR_PERMISSION_CONFIGURATION],
    'type' => MENU_CALLBACK,
    'file' => 'includes/admin_conf_server.inc',
  ];

  $items[CDSTAR_CONFIG_OBJECT_VIEW] = [
    'title callback' => 'cdstar_admin_conf_object_view_title',
    'description' => t('Display contents of a CDSTAR objects.'),
    'page callback' => 'cdstar_admin_conf_object_view',
    'page arguments' => [5],
    'access arguments' => [CDSTAR_PERMISSION_CONFIGURATION],
    'type' => MENU_CALLBACK,
    'file' => 'includes/admin_conf_object.inc',
  ];
  $items[CDSTAR_CONFIG_OBJECT_NEW] = [
    'title' => t('Add new CDSTAR Object'),
    'description' => t('Create a new CDSTAR object.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => ['cdstar_admin_conf_object_new'],
    'access arguments' => [CDSTAR_PERMISSION_CONFIGURATION],
    'type' => MENU_LOCAL_ACTION,
    'file' => 'includes/admin_conf_object.inc',
  ];
  $items[CDSTAR_CONFIG_OBJECT_DELETE] = [
    'title' => 'Delete CDSTAR server',
    'description' => '',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['cdstar_admin_conf_object_delete', 5],
    'access arguments' => [CDSTAR_PERMISSION_CONFIGURATION],
    'type' => MENU_CALLBACK,
    'file' => 'includes/admin_conf_object.inc',
  ];
  $items[CDSTAR_CONFIG_FILE_DELETE] = [
    'description' => 'Delete file',
    'page callback' => 'cdstar_admin_conf_file_delete',
    'page arguments' => [5, 6],
    'access arguments' => [CDSTAR_PERMISSION_CONFIGURATION],
    'type' => MENU_CALLBACK,
    'file' => 'includes/admin_conf_object.inc',
  ];
  $items[CDSTAR_CONFIG_FILE_DOWNLOAD] = [
    'description' => 'Download file',
    'page callback' => 'cdstar_admin_conf_file_download',
    'page arguments' => [5, 6],
    'access arguments' => [CDSTAR_PERMISSION_CONFIGURATION],
    'type' => MENU_CALLBACK,
    'file' => 'includes/admin_conf_object.inc',
  ];

  return $items;
}