<?php
/**
 * @file
 * Defines class CdstarObjectRepository.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CdstarObjectRepository
 */
class CdstarObjectRepository {

  private static $tableName = 'cdstar_object';

  private static $databaseFields = [
    'id',
    'server',
    'owner',
    'display_name',
  ];

  /* Cache ID (cid) for Drupal cache */
  private static $cache_id = 'cdstar';
  /**
   * Save object reference to database table and object payload to CDSTAR
   * service.
   *
   * @param CdstarObject $object
   *
   * @return bool|int Returns CDSTAR object ID on success, false otherwise.
   */
  public static function save(CdstarObject $object) {
    $server_id = $object->getServer();
    $server = CdstarServerRepository::findById($server_id);

    try {
      if ($object->isEmpty()) {

        // Get new CDSTAR object ID, return false to abort operation on failure.
        $id = self::getNewObjectId($server);
        if ($id == CdstarObject::EMPTY_ID) {
          return FALSE;
        }

        db_insert(self::$tableName)
          ->fields([
            'id' => $id,
            'server' => $server_id,
            'owner' => $object->getOwner(),
            'display_name' => $object->getDisplayName(),
          ])
          ->execute();
      }
      else {

        $id = $object->getId();

        db_update(self::$tableName)
          ->fields([
            'id' => $id,
            'server' => $server_id,
            'owner' => $object->getOwner(),
            'display_name' => $object->getDisplayName(),
          ])
          ->condition('id', $id, '=')
          ->execute();
      }

      // If no display_name is specified, use CDSTAR ID
      if (empty($object->getDisplayName())) {
        $object->setDisplayName($id);
      }

      // Store metadata
      $metadata = $object->getMetadata();
      if (!empty($metadata)) {
        CdstarAPI::createMetadata($server, $id, $metadata);
      }

      // Store permissions
      $permissions = $object->getPermissions();
      if (!empty($permissions)) {
        CdstarAPI::setPermissions($server, $id, $permissions);
      }

      // update internal cache
      self::cache($object);

      return $id;

    } catch (Exception $e) {
      watchdog_exception('CDSTAR', $e);
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  /**
   * Delete object reference from database and object payload from CDSTAR
   * service.
   *
   * @param CdstarObject $object
   *
   * @return bool True if object was deleted successfully.
   */
  public static function delete(CdstarObject $object) {
    try {
      $id = $object->getId();
      $server_id = $object->getServer();
      $server = CdstarServerRepository::findById($server_id);

      $request = CdstarAPI::deleteObject($server, $id);


      if (!$request['validation']) {
        return FALSE;
      }

      db_delete(self::$tableName)
        ->condition('id', $id)
        ->execute();

      // remove from cache
      self::rmCached($object);
      return TRUE;

    } catch (Exception $e) {
      watchdog_exception('CDSTAR', $e);
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  /**
   * Store a CdstarObject instance in Drupal cache.
   *
   * @param CdstarObject $object
   */
  public static function cache(CdstarObject $object) {
    if (self::caching()) {
      $key = $object->getId();

      if ($cache = cache_get(self::$cache_id)) {
        $data = $cache->data;
      }
      else {
        $data = [];
      }

      $data[$key] = $object;
      cache_set(self::$cache_id, $data);
    }
  }

  /**
   * Retrieve a cached CdstarObject from Drupal cache.
   *
   * @param string $object_id CDSTAR object ID
   *
   * @return CdstarObject|bool The stored CdstarObject instance, false on
   *   failure
   */
  private static function getCached($object_id) {
    if (self::caching()) {
      if ($cache = cache_get(self::$cache_id)) {
        if (array_key_exists($object_id, $cache->data)) {
          return $cache->data[$object_id];
        }
        else {
          return FALSE;
        }
      }
      else {
        return FALSE;
      }
    } else {
      return FALSE;
    }
  }

  /**
   * Delete object from cache.
   *
   * @param \CdstarObject $object
   */
  private static function rmCached(CdstarObject $object) {
    $object_id = $object->getId();
    if ($cache = cache_get(self::$cache_id)) {
      $data = $cache->data;
      if (array_key_exists($object_id, $data)) {
        unset($data[$object_id]);
        cache_set(self::$cache_id, $data);
      }
    }
  }

  /**
   * Query new CDSTAR object ID by creating a new object.
   *
   * @param CdstarServer $server
   *
   * @return string CDSTAR object ID
   */
  private static function getNewObjectId(CdstarServer $server) {
    $request = CdstarAPI::createObject($server);

    $result = $request['result'];

    if ($request['validation']) {
      $id = json_decode($result)->id;
      return $id;
    }
    else {
      return CdstarObject::EMPTY_ID;
    }
  }

  /**
   * Translate database results into CdstarObject instance and bootstrap stored
   * data from CDSTAR service.
   *
   * @param stdClass $result Generic object returned by database query.
   *
   * @return CdstarObject
   */
  private static function databaseResultToObject($result) {
    $object = new CdstarObject();

    if (empty($result)) {
      return $object;
    }

    // use internal cache if it has the object
    if ($cached_object = self::getCached($result->id)) {
      return $cached_object;
    }

    /************************** Pass database values **************************/

    $object->setId($result->id);
    $object->setServer($result->server);
    $object->setOwner($result->owner);
    $object->setDisplayName($result->display_name);

    /************************** Collect CDSTAR-stored data ********************/

    $server = CdstarServerRepository::findById($result->server);

    // Get object from CDSTAR storage (including metadata, permissions and files)
    $request = CdstarAPI::readObject($server, $result->id);
    $result = json_decode($request['result']);

    $object->setMetadata(json_encode($result->meta));
    $object->setPermissions(json_encode($result->acl));

    $files = $result->files;

    foreach ($files as $key => $value) {

      $cdstarfile = CdstarFileRepository::resultToFile($value);

      $cdstarfile->setObject($object->getId());
      $object->addFile($cdstarfile);
    }

    // update internal cache
    self::cache($object);

    return $object;
  }

  /**
   * Translate multiple database results into CdstarObject instances.
   *
   * @param stdClass[] $results Array of generic database query result objects.
   *
   * @return CdstarObject[] Array of CdstarObject instances.
   */
  private static function databaseResultsToObjects($results) {
    $objects = [];
    foreach ($results as $result) {
      $objects[] = self::databaseResultToObject($result);
    }

    return $objects;
  }


  /**
   * Fetch single CdstarObject by identifier (CDSTAR ID).
   *
   * @param string $id CDSTAR object ID.
   *
   * @return CdstarObject
   */
  public static function findById($id) {
    $result = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->condition('id', $id, '=')
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultToObject($result);
  }

  /**
   * Fetch CdstarObject(s) by owner.
   *
   * @param int $owner Drupal ID of user object.
   *
   * @return CdstarObject[] Array of CdstarObject instances.
   */
  public static function findByOwnerId($owner) {
    $result = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->condition('owner', $owner, '=')
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($result);
  }

  /**
   * Fetch CdstarObject(s) by storage service.
   *
   * @param int $server Drupal database ID of CdstarServer object.
   *
   * @return CdstarObject[] Array of CdstarObject instances.
   */
  public static function findByServerId($server) {
    $result = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->condition('server', $server, '=')
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($result);
  }

  /**
   * Fetch all registered CdstarObjects.
   *
   * @return CdstarObject[] Array of CdstarObject instances.
   */
  public static function findAll() {
    $results = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($results);
  }

  /**
   * Check if CDSTAR object caching is enabled in system settings.
   *
   * @return boolean TRUE if enabled.
   */
  private static function caching() {
    return variable_get('cdstar_config_caching', FALSE);
  }
}