<?php
/**
 * @file
 * Defines class CdstarFileRepository.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CdstarFileRepository
 */
class CdstarFileRepository {

  /**
   * Save the given CdstarFile within the associated CDSTAR storage object.
   *
   * @param CdstarFile $cdstarfile
   *
   * @return bool True if the file was stored successfully.
   */
  public static function save(CdstarFile $cdstarfile) {
    $object = CdstarObjectRepository::findById($cdstarfile->getObject());
    $server = CdstarServerRepository::findById($object->getServer());

    $request = CdstarAPI::saveFile($server, $cdstarfile);

    $result = json_decode($request['result']);

    if ($request['validation']) {

      // set the (normalized/hashed) filename under which
      // CDSTAR service stored the file as id
      $cdstarfile->setId($result->name);

      // append metadata
      // ToDo: handle invalid API response (?)
      CdstarAPI::createFileMetadata($server, $cdstarfile);

      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Delete the given CdstarFile from the associated CDSTAR storage object.
   *
   * @param CdstarFile $cdstarfile
   *
   * @return bool True if the file was deleted successfully.
   */
  public static function delete(CdstarFile $cdstarfile) {
    $object = CdstarObjectRepository::findById($cdstarfile->getObject());
    $server = CdstarServerRepository::findById($object->getServer());

    $request = CdstarAPI::deleteFile($server, $object->getId(), $cdstarfile);

    return $request['validation'];
  }

  /**
   * Fetch file from CDSTAR service, trigger automatic file download to client
   * browser. The file is not cached at the Drupal web server but streamed
   * directly from CDSTAR server to the client.
   *
   * @param CdstarFile $cdstarfile
   */
  public static function download(CdstarFile $cdstarfile) {
    try {
      $object = CdstarObjectRepository::findById($cdstarfile->getObject());
      $server = CdstarServerRepository::findById($object->getServer());

      // Set HTTP header for instant file download
      header('Content-Disposition: attachment; filename="' . $cdstarfile->getFilename() . '"');
      // Pass HTTP response to browser
      echo CdstarAPI::readFile($server, $object->getId(), $cdstarfile);

    } catch (Exception $e) {
      watchdog_exception('CDSTAR', $e);
      drupal_set_message($e->getMessage(), 'error');
    }
  }

  /**
   * Set \CdstarFile attributes from query returned \stdClass object.
   *
   * @param \stdClass $result Results from CDSTAR API query
   *
   * @return \CdstarFile
   */
  public static function resultToFile($result) {
    $cdstarfile = new CdstarFile();

    // normalized filename
    $cdstarfile->setId($result->name);

    // retrieve original filename from metadata
    $cdstarfile->setFilename($result->meta->{"dc:title"}[0]);

    $cdstarfile->setCreated(strtotime($result->created));
    $cdstarfile->setFilesize($result->size);
    $checksum_type = 'md5';
    $cdstarfile->setChecksum($result->digests->$checksum_type);
    $cdstarfile->setChecksumAlgorithm($checksum_type);
    $cdstarfile->setContenttype($result->type);
    $cdstarfile->setLastModified(strtotime($result->modified));

    return $cdstarfile;
  }
}