<?php
/**
 * @file
 * Defines abstract class CdstarAPI.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CdstarAPI (v3)
 *
 * Encapsulates communication with CDSTAR storage services' REST interface.
 */
abstract class CdstarAPI {

  /**
   * Prepare authentication parameters. (BasicAuth)
   *
   * @param CdstarServer $server
   *
   * @return string
   */
  private static function userpass(CdstarServer $server) {
    return $server->getUsername() . ':' . $server->getPassword();
  }

  /**
   * Prepare cURL handle and set common options.
   *
   * @param string $custom_method Http Request verb, i.e. GET, PUT, UPDATE.
   *   Default: POST.
   *
   * @return resource cURL handle
   */
  private static function initRequest(CdstarServer $server, $posturl, $custom_method = "") {
    $userpwd = self::userpass($server);

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_USERPWD, $userpwd);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

    curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    if (!empty($custom_method)) {
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $custom_method);
    }
    else {
      curl_setopt($ch, CURLOPT_POST, TRUE);
    }

    curl_setopt($ch, CURLOPT_URL, $posturl);

    return $ch;
  }

  /**
   *
   * @param resource $ch
   * cURL handle
   * @param string $posturl
   * Target URL
   * @param string $method
   * HTTP verb
   * @param array $valid_response_codes
   * HTTP response codes that will be regarded as successful operations.
   *
   * @return array Associative array providing the response code as key 'code'
   *   and the transmitted payload as 'result'
   */
  private static function executeRequest($ch, $posturl, $method,
                                         $valid_response_codes = [200, 201,]) {
    $result = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    $return = [
      'code' => $code,
      'result' => $result,
    ];
    if (in_array($code, $valid_response_codes)) {
      $return['validation'] = TRUE;
    }
    else {
      $return['validation'] = FALSE;
      watchdog('CDSTAR',
        t('HTTP :method call to :url returned code :code',
          [':method' => $method, ':url' => $posturl, ':code' => $code]),
        [], WATCHDOG_ERROR);
      drupal_set_message(t('An unexpected error occured in communication with CDSTAR storage service. 
                                                Please contact your system administrator'), 'error');
    }
    return $return;
  }

  /**
   * Create new CDSTAR storage object.
   *
   * @param CdstarServer $server
   *
   * @return array
   */
  public
  static function createObject(CdstarServer $server) {
    $method = 'POST';
    $posturl = $server->getUrl(); // v3 API

    $ch = self::initRequest($server, $posturl);

    return self::executeRequest($ch, $posturl, $method, [201]);
  }


  /**
   * Append metadata JSON string to existing CDSTAR storage object.
   *
   * @param CdstarServer $server
   * @param string $id CDSTAR object ID
   * @param string $metadata JSON string
   * @param bool $update True if metadata was already set before.
   *
   * @return array
   */
  public
  static function createMetadata(CdstarServer $server, $id, $metadata) {
    $method = 'POST';
    $posturl = $server->getUrl() . $id;

    $ch = self::initRequest($server, $posturl, $method);

    $data = [];

    foreach (json_decode($metadata) as $key => $value) {
      $data['meta:' . $key] = $value;
    }

    // ToDo: what are the viable metadata attributes???
    //$data = 'meta:dc:creator=' . json_decode($metadata)->author;
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

    return self::executeRequest($ch, $posturl, $method, [200]);
  }

  /**
   * Append file to existing CDSTAR storage object.
   * Works for create and update actions. (Updates file, if the same filename is
   * uploaded again to the same object/archive.
   *
   * @param CdstarServer $server
   * @param CdstarFile $cdstarfile
   *
   * @return array
   */
  public
  static function saveFile(CdstarServer $server, CdstarFile $cdstarfile) {
    $method = 'PUT';

    if ($cdstarfile->isEmpty()) {
      // prevent Umlaute etc. in filename
      $filename = $cdstarfile->normalized_filename();
    }
    else {
      $filename = $cdstarfile->getId();
    }

    $posturl = $server->getUrl() . $cdstarfile->getObject() . '/' . $filename;

    $ch = self::initRequest($server, $posturl, $method);

    // append file
    $tmpfile = $cdstarfile->getTmpfile();
    $infile = fopen($tmpfile, 'r');
    curl_setopt($ch, CURLOPT_UPLOAD, 1);
    curl_setopt($ch, CURLOPT_INFILE, $infile);
    curl_setopt($ch, CURLOPT_INFILESIZE, filesize($tmpfile));

    // append content-type information
    $header = ['Content-Type: ' . $cdstarfile->getContenttype()];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

    return self::executeRequest($ch, $posturl, $method, [200, 201]);
  }

  public
  static function createFileMetadata(CdstarServer $server, CdstarFile $cdstarFile) {
    $method = 'POST';

    $filename = $cdstarFile->getId();
    $posturl = $server->getUrl() . $cdstarFile->getObject();

    $ch = self::initRequest($server, $posturl, $method);

    $data = [];
    $metadata = $cdstarFile->getMetadata();
    foreach ($metadata as $key => $value) {
      $data['meta:' . $key . ':/' . $filename] = $value;
    }

    // ToDo: what are the viable metadata attributes???
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

    return self::executeRequest($ch, $posturl, $method, [200]);
  }

  /**
   * Get existing object from storage service.
   *
   * @param CdstarServer $server
   * @param string $id CDSTAR object ID
   *
   * @return array
   */
  public
  static function readObject(CdstarServer $server, $id) {
    $method = 'GET';

    // include metadata, permission and file list
    $posturl = $server->getUrl() . $id . '?with=files,meta,acl';

    $ch = self::initRequest($server, $posturl, $method);

    return self::executeRequest($ch, $posturl, $method);
  }

  /**
   * Read a specific file from CDSTAR storage. If the request is
   * successful, the binary file content is re-transmitted by the CDSTAR
   * service.
   *
   * @param CdstarServer $server
   * @param string $id CDSTAR object ID
   * @param CdstarFile $cdstarfile
   *
   * @return mixed Binary stream of file content on success.
   */
  public
  static function readFile(CdstarServer $server, $id, CdstarFile $cdstarfile) {
    $method = 'GET';

    $posturl = $server->getUrl() . $id . '/' . $cdstarfile->getId();

    $ch = self::initRequest($server, $posturl, $method);

    // Return binary file stream
    return curl_exec($ch);
  }

  /**
   * Read permissions of an existing CDSTAR storage object.
   *
   * @param CdstarServer $server
   * @param string $id CDSTAR object ID
   *
   * @return array
   */
  public
  static function readPermissions(CdstarServer $server, $id) {
    $method = 'GET';
    $posturl = $server->getUrl() . $id . '?acl';

    $ch = self::initRequest($server, $posturl, $method);

    return self::executeRequest($ch, $posturl, $method);
  }

  /**
   * Set permissions of an existing CDSTAR storage object.
   *
   * @param CdstarServer $server
   * @param string $id CDSTAR object ID
   * @param string $permissions JSON string of object permissions.
   *
   * @return array
   */
  public
  static function setPermissions(CdstarServer $server, $id, $permissions) {
    $method = 'PUT';
    $posturl = $server->getUrl() . $id . '?acl';

    $ch = self::initRequest($server, $posturl, $method);
    // ToDo: implement for v3
    $header = ['Content-Type: application/json'];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $permissions);

    return self::executeRequest($ch, $posturl, $method);
  }

  /**
   * Delete object from CDSTAR storage service.
   *
   * @param CdstarServer $server
   * @param string $id CDSTAR object ID
   *
   * @return array
   */
  public
  static function deleteObject(CdstarServer $server, $id) {
    $method = 'DELETE';
    $posturl = $server->getUrl() . $id;

    $ch = self::initRequest($server, $posturl, $method);

    // accept 404 "not found" response too: object did not exist (anymore)
    $valid_response_codes = [204, 404];

    return self::executeRequest($ch, $posturl, $method, $valid_response_codes);
  }

  /**
   * Delete a specific file from CDSTAR storage service object.
   *
   * @param CdstarServer $server
   * @param string $id CDSTAR object ID
   * @param CdstarFile $cdstarfile
   *
   * @return array
   */
  public
  static function deleteFile(CdstarServer $server, $id, CdstarFile $cdstarfile) {
    $method = 'POST';
    $posturl = $server->getUrl() . $id;
    $ch = self::initRequest($server, $posturl, $method);

    // specifiy file for deletion
    $data = 'delete:/' . $cdstarfile->getId() . '=/' . $cdstarfile->getId();
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

    return self::executeRequest($ch, $posturl, $method, [200]);
  }
}