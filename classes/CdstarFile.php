<?php
/**
 * @file
 * Defines class CdstarFile.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CdstarFile
 */
class CdstarFile {

  const EMPTY_ID = -1;

  /**
   * @var string
   */
  private $id = self::EMPTY_ID;

  /**
   * @var string CdstarObject ID
   */
  private $object;

  /**
   * @var string
   */
  private $filename;

  /**
   * @var string
   */
  private $tmpfile;

  /**
   * @var string MIME Type of the file.
   */
  private $content_type;

  /**
   * @var int Filesize in bytes.
   */
  private $filesize;

  /**
   * @var string Algorithmic checksum of the file.
   */
  private $checksum;

  /**
   * @var string The algorithm used to calculate the checksum.
   */
  private $checksum_algorithm;

  /**
   * @var int UTC timestamp of the file creation of the file.
   */
  private $created;

  /**
   * @var int UTC timestamp of the last modification of the file.
   */
  private $last_modified;

  /**
   * @var array of metadata attributes. Keyed by Dublin Core attributes, i.e.:
   * [ 'dc:creator' => 'Max Muster' ]
   */
  private $metadata = [];

  /**
   * Provide a HTML string of the file's attributes including links to download
   * and deletion callbacks.
   *
   * @return string
   */
  public function htmlDisplay() {
    $url = $this->url('download');
    $title = t('Download File');
    $download_icon = "<a href=\"$url\" target=\"_self\" data-toggle=\"tooltip\" title=\"$title\">
        <span class=\"glyphicon glyphicon-download\"></span></a>";
    $url = $this->url('delete');
    $title = t('Delete File');
    $delete_icon = "<a href=\"$url\" target=\"_self\" data-toggle=\"tooltip\" title=\"$title\"\">
        <span class=\"glyphicon glyphicon-trash\"></span></a>";

    $class_left = 'col-lg-4';
    $class_right = 'col-lg-8';

    $output = '<div class="col-sm-6">
                    <div class="container-fluid table-bordered" 
                        style="font-size: smaller; padding: 2px; margin: 2px; background-color: #f5f5f5;">
                        <div class="col-xs-12"><strong>' . $this->filename . '</strong>' . $download_icon . $delete_icon . '</div>
                        <div class="' . $class_left . '">Content-Type:</div>
                        <div class="' . $class_right . '">' . $this->getContenttype() . '</div>
                        <div class="' . $class_left . '">Filesize:</div>
                        <div class="' . $class_right . '">' . format_size($this->getFilesize()) . '</div>
                        <div class="' . $class_left . '">Created:</div>
                        <div class="' . $class_right . '">' . date("Y-m-d H:i:s", $this->getCreated()) . '</div>
                        <div class="' . $class_left . '">Last modified:</div>
                        <div class="' . $class_right . '">' . date("Y-m-d H:i:s", $this->getLastModified()) . '</div>
                        <div class="' . $class_left . '">Checksum:</div>
                        <div class="' . $class_right . '">' . $this->getChecksum() . '</div>
                    </div>
                   </div>';
    return $output;
  }

  /**
   * Save the CdstarFile object to repository.
   *
   * Filename, content type and temporary file pointer must be set.
   *
   * @return bool True if file was successfully saved to repository.
   */
  public function save() {
    if (empty($this->filename) || empty($this->content_type)) {
      drupal_set_message(t('Filename or Mime-Type not set.'), 'error');
      return FALSE;
    }

    if (empty($this->tmpfile)) {
      drupal_set_message(t('Temporary uploaded file not set.'), 'error');
      return FALSE;
    }

    if (CdstarFileRepository::save($this)) {

      // Delete temporary uploaded file pointer
      unset($this->tmpfile);

      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * @return bool True if file was deleted successfully from Repository.
   */
  public function delete() {
    return CdstarFileRepository::delete($this);
  }

  /**
   * Triggers file download.
   */
  public function download() {
    CdstarFileRepository::download($this);
  }

  /**
   * Checks whether CdstarFile instance is empty.
   * If a instance is empty, that means, that it has not yet been stored into
   * database.
   *
   * @return bool True if empty, i.e. instance has not yet been stored.
   */
  public function isEmpty() {
    if ($this->id == self::EMPTY_ID) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }


  /**
   * Build and return different URL paths.
   *
   * @param string $route A string specifying which functional path is desired.
   *
   * @return string
   */
  public function url($route) {
    switch ($route) {
      case 'delete':
        return url(CDSTAR_CONFIG_OBJECT_DEFAULT . '/' . $this->object .
          '/' . $this->id . '/delete');
      case 'download':
        return url(CDSTAR_CONFIG_OBJECT_DEFAULT . '/' . $this->object .
          '/' . $this->id);
    }
  }

  /**
   * Replace special characters (Umlaute) and remove spaces from filename.
   *
   * @return string
   */
  public function normalized_filename() {
    // Replace special chars with standard alphanumerical values
    //$filename = unidecode($this->filename);
    // Further remove whitespaces
    //$filename = preg_replace('/[^a-zA-Z0-9.-_]/', '', $filename);

    // calculate a hash value and use the first 12 characters
    $filename = substr(md5($this->filename), 0, 12);

    return $filename;
  }

  /*************************** GETTERS AND SETTERS ****************************/

  /**
   * @param string $id
   */

  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param mixed $filename
   */
  public function setFilename($filename) {
    $this->filename = $filename;
    $this->metadata['dc:title'] = $filename;
  }

  /**
   * @return mixed
   */
  public function getFilename() {
    return $this->filename;
  }

  /**
   * @param mixed $content_type
   */
  public function setContenttype($content_type) {
    $this->content_type = $content_type;
    $this->metadata['dc:FileFormat'] = $content_type;
  }

  /**
   * @return mixed
   */
  public function getContenttype() {
    return $this->content_type;
  }

  /**
   * @param mixed $tmpfile
   */
  public function setTmpfile($tmpfile) {
    $this->tmpfile = $tmpfile;
  }

  /**
   * @return mixed
   */
  public function getTmpfile() {
    return $this->tmpfile;
  }

  /**
   * @return int CdstarObject::id
   */
  public function getObject() {
    return $this->object;
  }

  /**
   * @param int $object The ID of a CdstarObject
   */
  public function setObject($object_id) {
    $this->object = $object_id;
  }


  /**
   * @return mixed
   */
  public function getFilesize() {
    return $this->filesize;
  }

  /**
   * @param mixed $filesize
   */
  public function setFilesize($filesize) {
    $this->filesize = $filesize;
    $this->metadata['dc:size'] = $filesize;
  }

  /**
   * @return mixed
   */
  public function getChecksum() {
    return $this->checksum;
  }

  /**
   * @param mixed $checksum
   */
  public function setChecksum($checksum) {
    $this->checksum = $checksum;
  }

  /**
   * @return mixed
   */
  public function getCreated() {
    return $this->created;
  }

  /**
   * @param mixed $created
   */
  public function setCreated($created) {
    $this->created = $created;
  }

  /**
   * @return mixed
   */
  public function getChecksumAlgorithm() {
    return $this->checksum_algorithm;
  }

  /**
   * @param mixed $checksum_algorithm
   */
  public function setChecksumAlgorithm($checksum_algorithm) {
    $this->checksum_algorithm = $checksum_algorithm;
  }

  /**
   * @return mixed
   */
  public function getLastModified() {
    return $this->last_modified;
  }

  /**
   * @param mixed $last_modified
   */
  public function setLastModified($last_modified) {
    $this->last_modified = $last_modified;
  }

  /**
   * @return array
   */
  public function getMetadata() {
    return $this->metadata;
  }

  /**
   * @param array $metadata
   */
  public function setMetadata($metadata) {
    $this->metadata = $metadata;
  }

}