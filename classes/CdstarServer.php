<?php
/**
 * @file
 * Defines class CdstarServer.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CdstarServer
 */
class CdstarServer {

  /**
   * Constant identifier for unsaved objects.
   */
  const EMPTY_ID = -1;

  /**
   * Table column headers for simple table display with Drupal table theme
   */
  const tableHeader = [
    'Server',
    'URL',
    'Username',
    'Options',
  ];

  /**
   * @var int
   */
  private $id = self::EMPTY_ID;

  /**
   * @var string
   */
  private $name;

  /**
   * @var string
   */
  private $label;

  /**
   * @var string
   */
  private $url;

  /**
   * @var string
   */
  private $username;

  /**
   * @var string
   */
  private $password;

  /**
   * @return string JSON encoded array representation of the object.
   */
  public function __toString() {
    $json = [
      $this->name => [
        'id' => $this->id,
        'label' => $this->label,
        'url' => $this->url,
        'username' => $this->username,
        'password' => $this->password,
      ],
    ];
    return json_encode($json);
  }

  /**
   * @return array human-readable data fields
   * for simple table row display with Drupal table theme.
   */
  public function tableRow() {
    $edit_icon = '<a href="' . $this->url('edit') . '">
          <span class="glyphicon glyphicon-edit"></span>
        </a>';

    $delete_icon = '<a href="' . $this->url('delete') . '">
          <span class="glyphicon glyphicon-trash"></span>
        </a>';

    return [
      $this->label . ' (' . $this->name . ')',
      $this->url,
      $this->username,
      $edit_icon . $delete_icon,
    ];
  }

  /**
   * @return bool
   */
  public function isEmpty() {
    if ($this->id == self::EMPTY_ID) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }


  /**
   * Generates the Drupal form field array for a specified class variable (the
   * parameter)
   *
   * @param $fieldname
   *
   * @return array Drupal form field
   */
  public function getFormField($fieldname) {
    switch ($fieldname) {
      case 'name':
        return [
          '#type' => 'textfield',
          '#title' => t('Server machine name'),
          '#description' => t('Enter the machine name of the new CDSTAR server.'),
          '#default_value' => $this->name,
          '#required' => TRUE,
        ];
      case 'label':
        return [
          '#type' => 'textfield',
          '#title' => t('Server label'),
          '#description' => t('Enter a human readable name for the server.'),
          '#default_value' => $this->label,
          '#required' => TRUE,
        ];
      case 'url':
        return [
          '#type' => 'textfield',
          '#title' => t('Server URL'),
          '#description' => t('Enter the base URL for API calls to the server.'),
          '#default_value' => $this->url,
          '#required' => TRUE,
        ];
      case 'username':
        return [
          '#type' => 'textfield',
          '#title' => t('Username'),
          '#description' => t('Enter a username for the connection to the server.'),
          '#default_value' => $this->username,
          '#required' => TRUE,
        ];
      case 'password':
        return [
          '#type' => 'password',
          '#title' => t('Password'),
          '#description' => t('Enter a password for the connection to the server.'),
        ];
    }
  }

  /**
   * Stores object in repository.
   */
  public function save() {
    $id = CdstarServerRepository::save($this);

    // Update id value if was empty before and repository saving was successful.
    if ($this->isEmpty() && $id) {
      $this->id = $id;
    }
  }

  /**
   * Deletes object from repository.
   *
   * @return bool
   */
  public function delete() {
    return CdstarServerRepository::delete($this);
  }

  /**
   * Build and return different URL paths.
   *
   * @param string $route A string specifying which functional path is desired.
   *
   * @return string
   */
  public function url($route = 'edit') {
    switch ($route) {
      case 'edit':
        return url(CDSTAR_CONFIG_SERVER_DEFAULT . '/' . $this->id . '/edit');
      case 'delete':
        return url(CDSTAR_CONFIG_SERVER_DEFAULT . '/' . $this->id . '/delete');
    }
  }

  /************************************* GETTERS AND SETTERS **************************************/

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $label
   */
  public function setLabel($label) {
    $this->label = $label;
  }

  /**
   * @return mixed
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * @param mixed $url
   */
  public function setUrl($url) {
    $this->url = $url;
  }

  /**
   * @return mixed
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * @param mixed $username
   */
  public function setUsername($username) {
    $this->username = $username;
  }

  /**
   * @return mixed
   */
  public function getUsername() {
    return $this->username;
  }

  /**
   * @param mixed $password
   */
  public function setPassword($password) {
    $this->password = $password;
  }

  /**
   * @return mixed
   */
  public function getPassword() {
    return $this->password;
  }

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }
}