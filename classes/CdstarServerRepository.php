<?php
/**
 * @file
 * Defines class CdstarServerRepository.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CdstarServerRepository
 */
class CdstarServerRepository {

  static $tableName = 'cdstar_server';

  static $databaseFields = [
    'id',
    'name',
    'label',
    'url',
    'username',
    'password',
  ];

  /**
   * Save CdstarServer instance to database.
   *
   * @param CdstarServer $server
   *
   * @return bool|int Server ID if successfully inserted, true if successfully
   *   updated, false otherwise.
   */
  public static function save(CdstarServer $server) {
    try {
      if ($server->isEmpty()) {
        $id = db_insert(self::$tableName)
          ->fields([
            'name' => $server->getName(),
            'label' => $server->getLabel(),
            'url' => $server->getUrl(),
            'username' => $server->getUsername(),
            'password' => $server->getPassword(),
          ])
          ->execute();
        return (int) $id;

      }
      else {
        db_update(self::$tableName)
          ->fields([
            'id' => $server->getId(),
            'name' => $server->getName(),
            'label' => $server->getLabel(),
            'url' => $server->getUrl(),
            'username' => $server->getUsername(),
            'password' => $server->getPassword(),
          ])
          ->condition('id', $server->getId(), '=')
          ->execute();
        return TRUE;
      }
    } catch (Exception $e) {
      watchdog_exception('CDSTAR', $e);
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  /**
   * Translate database result to class instance.
   *
   * @param stdClass $result Generic database result object.
   *
   * @return CdstarServer
   */
  private static function databaseResultToObject($result) {
    $server = new CdstarServer();

    if (empty($result)) {
      return $server;
    }

    $server->setId($result->id);
    $server->setName($result->name);
    $server->setLabel($result->label);
    $server->setUrl($result->url);
    $server->setUsername($result->username);
    $server->setPassword($result->password);

    return $server;
  }

  /**
   * Translate mupltiple database results to class instances.
   *
   * @param stdClass[] $results Array of generic database result objects.
   *
   * @return CdstarServer[] Array of CdstarServer instances.
   */
  private static function databaseResultsToObjects($results) {
    $servers = [];
    foreach ($results as $result) {
      $servers[] = self::databaseResultToObject($result);
    }

    return $servers;
  }

  /**
   * Fetch instances from database by identifier.
   *
   * @param int $id CdstarServer identifier.
   *
   * @return CdstarServer
   */
  public static function findById($id) {
    $result = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->condition('id', $id, '=')
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultToObject($result);

  }

  /**
   * Fetch all CdstarServer instances from database.
   *
   * @return CdstarServer[]
   */
  public static function findAll() {
    $results = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($results);
  }

  /**
   * Delete instance from database.
   *
   * @param CdstarServer $server
   *
   * @return bool True if deleted successfully.
   */
  public static function delete(CdstarServer $server) {
    try {
      db_delete(self::$tableName)
        ->condition('id', $server->getId())
        ->execute();

      return TRUE;

    } catch (Exception $e) {
      watchdog_exception('CDSTAR', $e);
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }
}
