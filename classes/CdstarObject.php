<?php
/**
 * @file
 * Defines class CdstarObject.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CdstarObject
 */
class CdstarObject {

  /**
   * Constant identifier for unsaved objects.
   */
  const EMPTY_ID = -1;

  /**
   * Table column headers for simple table display with Drupal table theme
   */
  const tableHeader = [
    'CDSTAR Object',
    'Server',
    'Unique Identifier',
    'Owner',
    'Options',
  ];

  /**
   * @var string Unique Identifier assigned to the object by CDSTAR server
   */
  private $id;

  /**
   * @var int CdstarServer::id of the corresponding server configuration
   */
  private $server;

  /**
   * @var string A manually set display name for the object.
   */
  private $display_name;

  /**
   * @var int Drupal id of the user that created the object
   */
  private $owner;

  /**
   * @var string JSON encoded array
   */
  private $metadata;

  /**
   * @var string JSON encoded array
   */
  private $permissions;

  /**
   *
   * @var CdstarFile[] Associative array of CdstarFile instances. File IDs
   *   are array keys, CdstarFile objects the array values
   */
  private $files = [];

  /**
   * CdstarObject constructor.
   */
  public function __construct() {
    $this->id = self::EMPTY_ID;

    global $user;
    $this->owner = $user->uid;
  }

  /**
   * @return bool
   */
  public function isEmpty() {
    if ($this->id == self::EMPTY_ID) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * @return array human-readable data fields for simple table row display with
   *   Drupal table theme.
   */
  public function tableRow() {
    $serverlabel = CdstarServerRepository::findById($this->server)->getLabel();
    $ownerlabel = user_load($this->owner)->name;

    $view_icon = '<a href="' . $this->url() . '" 
                            target="_self" data-toggle="tooltip" title="' . t('View') . '">
                          <span class="glyphicon glyphicon-expand"></span>
                        </a>';
    $delete_icon = '<a href="' . $this->url('delete') . '" 
                            target="_self" data-toggle="tooltip" title="' . t('Delete') . '">
                          <span class="glyphicon glyphicon-trash"></span>
                        </a>';
    $link_icon = '<a href="' . $this->url('landing') . '" 
                            target="_blank" data-toggle="tooltip" title="' . t('External Link') . '">
                          <span class="glyphicon glyphicon-link"></span>
                        </a>';

    return [
      l($this->display_name, $this->url()),
      $serverlabel,
      $this->id,
      $ownerlabel,
      $view_icon . $link_icon . $delete_icon,
    ];
  }

  /**
   * @return string HTML representation of associated files.
   */
  public function listFiles() {
    if (count($this->files) == 0) {
      $output = '<p>' . t('No associated files.') . '</p>';
    }
    else {

      $output = '<div class"container-fluid"><div class="row">';
      foreach ($this->files as $key => $value) {
        $output .= $value->htmlDisplay();
      }
      $output .= '</div></div>';
    }
    return $output;
  }

  /**
   * Stores object in repository.
   */
  public function save() {
    $id = CdstarObjectRepository::save($this);

    if ($this->isEmpty()) {
      $this->id = $id;
    }
  }

  /**
   * Deletes object and attached files from database and CDSTAR storage service.
   *
   * @return boolean true if deleted successfully
   */
  public function delete() {
    return CdstarObjectRepository::delete($this);
  }

  /**
   * Fetch object with a given identifier.
   *
   * @param string $object_id
   *
   * @return CdstarObject
   */
  public static function getObject($object_id) {
    return CdstarObjectRepository::findById($object_id);
  }

  /**
   * Build and return different URL paths.
   *
   * @param string $route A string specifying which functional path is desired.
   *
   * @return string
   */
  public function url($route = 'view') {
    switch ($route) {
      case 'view':
        return url(CDSTAR_CONFIG_OBJECT_DEFAULT . '/' . $this->id);
      case 'delete':
        return url(CDSTAR_CONFIG_OBJECT_DEFAULT . '/' . $this->id . '/delete');
      case 'landing':
        // CDSTAR server landing page (external link)
        return CdstarServerRepository::findById($this->server)
            ->getUrl() . 'landing/' . $this->id;
    }
  }

  /**
   * Generates the Drupal form field array for a specified class variable (the
   * parameter)
   *
   * @param $fieldname
   *
   * @return array Drupal form field
   */
  public function getFormField($fieldname) {
    switch ($fieldname) {
      case 'display_name':
        return [
          '#type' => 'textfield',
          '#title' => t('Display name'),
          '#description' => t('Enter a display name for the CDSTAR object. 
                        The display name can be arbitrary and has no effect on uploaded files.'),
          '#default_value' => $this->display_name,
        ];
      case 'server':
        $servers = CdstarServerRepository::findAll();
        $options = [];
        foreach ($servers as $server){
          $options[$server->getId()] = $server->getLabel();
        }
        return [
          '#type' => 'select',
          '#title' => t('CDSTAR Server'),
          '#description' => t('Choose a CDSTAR storage service.'),
          '#options' => $options,
          '#required' => TRUE,
        ];
    }
  }


  /**
   * ToDo: doc
   *
   * @return array
   */
  public static function getFormAddFile() {
    $form = [];

    $form['cdstar_file'] = [
      '#type' => 'file',
      '#title' => 'Add new file.',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Upload'),
    ];

    $form['#submit'][] = 'cdstar_object_addfile_form_submit';

    return $form;
  }

  /**
   * ToDo: doc
   *
   * @param $form
   * @param $form_state
   */
  public function addFileFormAction($form, &$form_state) {
    $cdstarfile = new CdstarFile();

    // Pass temporarily-uploaded file data
    $cdstarfile->setFilename($_FILES['files']['name']['cdstar_file']);
    $cdstarfile->setContenttype($_FILES['files']['type']['cdstar_file']);
    $cdstarfile->setTmpfile($_FILES['files']['tmp_name']['cdstar_file']);

    $cdstarfile->setObject($this->id);

    if ($cdstarfile->save()) {
      // Add successfully saved file to this object's list.
      $this->addFile($cdstarfile);
    }
    $form_state['redirect'] = $this->url();
  }

  /**
   * Add file to private list of attached files.
   * NB: Cache must be updated after execution.
   *
   * @param CdstarFile $cdstarfile
   */
  public function addFile(CdstarFile $cdstarfile) {
    if (!$cdstarfile->isEmpty()) {
      $this->files[$cdstarfile->getId()] = $cdstarfile;
    }
    // update cache
    $this->cache();
  }

  /**
   * Remove file from private list of attached files.
   * NB: Cache must be updated after execution.
   *
   * @param CdstarFile $cdstarfile
   *
   * @return bool true if file was successfully deleted
   */
  public function removeFile(CdstarFile $cdstarfile) {
    $id = $cdstarfile->getId();
    if (array_key_exists($id, $this->files)) {
      if ($cdstarfile->delete()) {
        unset($this->files[$id]);
        // update cache
        $this->cache();
        return TRUE;
      }
    }

    // Something went wrong...
    return FALSE;
  }

  /**
   * Store/update CdstarObject instance in cache.
   */
  private function cache() {
    CdstarObjectRepository::cache($this);
  }

  /************************************* GETTERS AND SETTERS **************************************/

  /**
   * @return array Associative array of CdstarFile objects:
   *              [CdstarFile::id => CdstarFile]
   */
  public function getFiles() {
    return $this->files;
  }

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param string $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return mixed
   */
  public function getServer() {
    return $this->server;
  }

  /**
   * @param mixed $server
   */
  public function setServer($server) {
    $this->server = $server;
  }

  /**
   * @return mixed
   */
  public function getMetadata() {
    return $this->metadata;
  }

  /**
   * @param mixed $metadata
   */
  public function setMetadata($metadata) {
    $this->metadata = $metadata;
  }

  /**
   * @return mixed
   */
  public function getOwner() {
    return $this->owner;
  }

  /**
   * @param mixed $owner
   */
  public function setOwner($owner) {
    $this->owner = $owner;
  }

  /**
   * @return string
   */
  public function getDisplayName() {
    return $this->display_name;
  }

  /**
   * @param string $display_name
   */
  public function setDisplayName($display_name) {
    $this->display_name = $display_name;
  }

  /**
   * @return string
   */
  public function getPermissions() {
    return $this->permissions;
  }

  /**
   * @param string $permissions
   */
  public function setPermissions($permissions) {
    $this->permissions = $permissions;
  }


}