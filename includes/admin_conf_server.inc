<?php
/**
 * @file
 * Callbacks, forms and functions to manage CdstarServer instances in Drupal
 *   admin UI.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Displays CDSTAR_CONFIG_SERVER_DEFAULT path.
 * A list of configured servers, links to the respective paths
 * to edit, delete and add new server configurations.
 *
 * @return string Drupal page content
 */
function cdstar_admin_conf_server() {
  // Get existing servers
  $servers = CdstarServerRepository::findAll();

  // Build table of configured servers
  $rows = [];
  foreach ($servers as $server) {
    $rows[] = $server->tableRow();
  }
  $header = CdstarServer::tableHeader;
  try {
    $output = theme('table', ['header' => $header, 'rows' => $rows]);
  } catch (Exception $e) {
    watchdog_exception('CDSTAR', $e);
  }

  // Add button to create new server
  $attributes = [
    'attributes' => [
      'class' => 'btn btn-success',
      'role' => 'button',
    ],
  ];
  $output .= l(t('Add new CDSTAR server'), CDSTAR_CONFIG_SERVER_NEW, $attributes);

  return $output;
}

/**
 * Drupal Form to add new CdstarServer object
 *
 * @return array Drupal Form
 */
function cdstar_admin_conf_server_new() {
  $form = [];
  $server = new CdstarServer();

  $form['name'] = $server->getFormField('name');
  $form['label'] = $server->getFormField('label');
  $form['url'] = $server->getFormField('url');

  $form['nb'] = [
    '#markup' => '<p><strong>NB: Only BaseAuth option allowed at the moment!</strong></p>',
  ];

  $form['username'] = $server->getFormField('username');
  $form['password'] = $server->getFormField('password');
  $form['password']['#required'] = TRUE;

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Add new Server'),
  ];

  $form['cancel'] = [
    '#type' => 'button',
    '#submit' => ['cdstar_admin_conf_server_cancel'],
    '#value' => t('Cancel'),
    '#executes_submit_callback' => TRUE,
    '#limit_validation_errors' => [],
  ];

  $form['#submit'][] = 'cdstar_admin_conf_server_new_submit';

  return $form;
}

/**
 * Handles new CdstarServer object form submit action
 *
 * @param $form
 * @param $form_state
 */
function cdstar_admin_conf_server_new_submit($form, &$form_state) {
  $values = $form_state['values'];

  $server = new CdstarServer();

  $server->setName($values['name']);
  $server->setLabel($values['label']);
  $server->setUrl($values['url']);
  $server->setUsername($values['username']);
  $server->setPassword($values['password']);

  $server->save();

  $form_state['redirect'] = CDSTAR_CONFIG_SERVER_DEFAULT;
}

/**
 * Drupal Form to add new CdstarServer object
 *
 * @param $form
 * @param $form_state
 * @param $server_id int CdstarServer object ID from path
 *
 * @return array Drupal Form
 */
function cdstar_admin_conf_server_edit($form, &$form_state, $server_id) {
  $server = CdstarServerRepository::findById($server_id);
  $form = [];

  $form_state['server_id'] = $server_id;

  $form['name'] = $server->getFormField('name');
  $form['label'] = $server->getFormField('label');
  $form['url'] = $server->getFormField('url');

  $form['nb'] = [
    '#markup' => '<p><strong>NB: Only BaseAuth option allowed at the moment!</strong></p>',
  ];

  $form['username'] = $server->getFormField('username');
  $form['password'] = $server->getFormField('password');

  $form['cancel'] = [
    '#type' => 'button',
    '#submit' => ['cdstar_admin_conf_server_cancel'],
    '#value' => t('Cancel'),
    '#executes_submit_callback' => TRUE,
    '#limit_validation_errors' => [],
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Update Server settings'),
  ];

  $form['#submit'][] = 'cdstar_admin_conf_server_edit_submit';

  return $form;
}


/**
 * Handles CdstarServer object edit form submit action
 *
 * @param $form
 * @param $form_state
 */
function cdstar_admin_conf_server_edit_submit($form, &$form_state) {
  $values = $form_state['values'];

  $server = CdstarServerRepository::findById($form_state['server_id']);

  $server->setName($values['name']);
  $server->setLabel($values['label']);
  $server->setUrl($values['url']);
  $server->setUsername($values['username']);
  if (!empty($values['password'])) {
    $server->setPassword($values['password']);
  }
  $server->save();

  $form_state['redirect'] = CDSTAR_CONFIG_SERVER_DEFAULT;
}

/**
 * Delete CdstarServer entry.
 * Implements @see hook_form().
 *
 * @param $form
 * @param $form_state
 * @param int $server_id The database ID of the entry..
 *
 * @return array
 */
function cdstar_admin_conf_server_delete($form, &$form_state, $server_id) {
  if (count(CdstarObjectRepository::findByServerId($server_id)) > 0) {
    drupal_set_message(t('Cannot delete CDSTAR server configuration 
            because at least one object is stored with the given server.'), 'warning');
    drupal_goto(CDSTAR_CONFIG_SERVER_DEFAULT);
  }
  else {
    $form = [];
    $form_state['server_id'] = $server_id;

    $server = CdstarServerRepository::findById($server_id);
    $form['nb'] = [
      '#markup' => '<p>' . t("Delete CDSTAR Server :server permanently?",
          [':server' => $server->getName()]) . '</p>',
    ];

    $form['cancel'] = [
      '#type' => 'button',
      '#submit' => ['cdstar_admin_conf_server_cancel'],
      '#value' => t('Cancel'),
      '#executes_submit_callback' => TRUE,
      '#limit_validation_errors' => [],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Delete server'),
    ];

    $form['#submit'][] = 'cdstar_admin_conf_server_delete_submit';

    return $form;
  }
}


/**
 * Handles the delete form action, deletes \CdstarServer entry from repository.
 *
 * @param $form
 * @param $form_state
 */
function cdstar_admin_conf_server_delete_submit($form, &$form_state) {
  $server_id = $form_state['server_id'];
  $server = CdstarServerRepository::findById($server_id);
  if ($server->delete()) {
    drupal_set_message(t("Successfully deleted CDSTAR server configuration :server.",
      [':server' => $server->getName()]));
  }

  $form_state['redirect'] = CDSTAR_CONFIG_SERVER_DEFAULT;
}

/**
 * Handles CdstarServer form cancel action
 *
 * @param $form
 * @param $form_state
 */
function cdstar_admin_conf_server_cancel($form, &$form_state) {
  $form_state['redirect'] = CDSTAR_CONFIG_SERVER_DEFAULT;
}