<?php
/**
 * @file
 * Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

function cdstar_admin_conf() {
  $form = [];

  $default = variable_get('cdstar_config_caching', FALSE);

  $form['cache'] = [
    '#type' => 'fieldset',
    '#title' => t('CDSTAR Module Cache'),
  ];

  $form['cache']['description'] = [
    '#markup' => t('Enabling CDSTAR module-specific cache will reduce 
      communication with the storage server and improve load times. When caching
      is enabled, objects will only requested from the storage service when 
      changes have been commited by the user.'),
  ];

  $form['cache']['cdstar_config_caching'] = [
    '#title' => 'Enable CDSTAR Object Cache',
    '#description' => 'Enable or disable CDSTAR module caching.',
    '#type' => 'checkbox',
    '#default_value' => $default,
  ];

  return system_settings_form($form);
}